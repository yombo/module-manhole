Manhole
==================

This adds the twisted manhole to Yombo Gateway. This can be used to inspect
various items during runtime of the gateway.  Only use this module for testing
purposes and can pose a huge security risk if left on.

**Caution**: This is a high risk module to leave installed. It allows direct
access to the python interactive shell. Any user that gains access to this
can do *anything* on your computer.

**Caution**: Read the above caution again!!!

Installation
============

Simply mark this module as being used by the gateway, and the gateway will
download this module and install this module automatically.

Requirements
============

None.

Usage
=====

After you telnet to the correct port, you will be prompted for username/password.
Once access is granted, you are dropped to a python interactive shell that can
access any gateway component and variable.  The example below shows all the
variables of the time library and any current states.

::

  telnet localhost 5560`
  Trying 127.0.0.1...
  Connected to localhost.
  Escape character is '^]'.


        ************************
        * Yombo Manhole Module *
        ************************

  username: admin
  password: *****

  >>> self.components['yombo.gateway.lib.times'].__dict__
  {'CLnowDusk': None, 'CLnowNotDusk': None, 'isDay': False, 
  'CLnowNight': <twisted.internet.base.DelayedCall instance at 0x193e170>, 'isNight': True,
  'isDark': False, 'CLnowLight': <twisted.internet.base.DelayedCall instance at 0x193e050>,
  'CLnowNotDawn': None, 'CLnowDark': None, 'loader': <yombo.lib.loader.Loader instance at 0x1602cf8>,
  'isDawn': None, 'obsTwilight': <ephem.Observer date='2013/7/29 22:28:00' epoch='2000/1/1 12:00:00'
  lon=-121:17:49.1 lat=38:35:46.0 elevation=29.0m horizon=-6:00:00.0 temp=15.0C pressure=1010.0mBar>,
  'isDusk': None, 'isLight': True, 'isTwilight': False, 'CLnowDay': None,
  'CLnowDawn': <twisted.internet.base.DelayedCall instance at 0x193e128>, '_Name': 'Times',
  '_FullName': 'yombo.gateway.lib.Times', 'obs': <ephem.Observer date='2013/7/29 22:28:00'
  epoch='2000/1/1 12:00:00' lon=-121:17:49.1 lat=38:35:46.0 elevation=29.0m horizon=0:00:00.0 temp=15.0C pressure=1010.0mBar>}
  

License
=======

This module is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The `Yombo.net <http://www.yombo.net/>`_ team and other contributors
hopes that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

The GNU General Public License can be found here: `GNU.Org <http://www.gnu.org/licenses>`_

