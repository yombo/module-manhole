"""
Setups an twisted manhole into the Yombo Gateway.  Used for inspecting various
gateway components during run time.

:copyright: 2012 Yombo
:license: GPL
"""
#from twisted.internet import reactor
from twisted.manhole import telnet
from twisted.internet import reactor

from yombo.core.module import YomboModule
from yombo.core.helpers import getTimes, generateRandom
from yombo.core.log import getLogger
from yombo.lib.loader import getTheLoadedComponents

logger = getLogger("module.manhole")

class Manhole(YomboModule):
    """
    Manhole module
    """
    def _init_(self):
        self._ModDescription = "Manhole module for Yombo Gateway"
        self._ModAuthor = "Mitch Schwenk @ Yombo"
        self._ModUrl = "http://www.yombo.net"
        self._RegisterDistributions = ["all"]

       
    def _load_(self):
        """
        Setup the telnet port, and make a pointer to all 'components'.
        """
        self.mhole = telnet.ShellFactory()
        self.mhole.protocol = Shell
        self.mhole.password = "helloboss"
        self.mhole.namespace['self'] = self

        if "username" in self._ModVariables:
          self.mhole.username = self._ModVariables["username"][0]
        else:
          logger.warning("No 'username' set for manhole login id, using default: admin")
          self.mhole.username = "admin"

        if "password" in self._ModVariables:
          self.mhole.password = self._ModVariables["password"][0]
        else:
#          self.mhole.password = generateRandom(length=10)
          self.mhole.password = "letmein"
          logger.warning("No 'password' set for manhole, setting temp password: %s" % self.mhole.password)

        if "port" in self._ModVariables:
          self.port = self._ModVariables["port"][0]
        else:
          logger.warning("No 'port' set for manhole listening port, using default: 5560")
          self.port = 5560

        self.manhole = reactor.listenTCP(self.port, self.mhole, interface = "127.0.0.1")
        self.manhole.interface = "127.0.0.1"

        self.components = getTheLoadedComponents()
        self.mhole.namespace['loader'] = self.components['yombo.gateway.lib.loader']
        self.mhole.namespace['devices'] = self.components['yombo.gateway.lib.devices']
        self.mhole.namespace['commands'] = self.components['yombo.gateway.lib.commands']
        self.mhole.namespace['messages'] = self.components['yombo.gateway.lib.messages']
        self.mhole.namespace['modules'] = self.components['yombo.gateway.lib.loader'].moduleNames
        self.mhole.namespace['libraries'] = self.components['yombo.gateway.lib.loader'].libraryNames
        self.mhole.namespace['times'] = self.components['yombo.gateway.lib.times']

        devlist = {}
        for dev in self.components['yombo.gateway.lib.devices'].yombodevices:
            devlist[dev] = self.components['yombo.gateway.lib.devices'].yombodevices[dev]

        self.mhole.namespace['devlist'] = devlist
        
    def _start_(self):
        """
        Nothing to start.
        """
        pass
    
    def _stop_(self):
        """
        Disconnection clients and stop the listener.
        """
        self.manhole.stopListening()
    
    def _unload_(self):
        """
        Nothing to unload.
        """
        pass

    def message(self, message):
        """
        For now, do nothing with messages.
        """
        pass
        
        
class ManholeFactory(telnet.ShellFactory):
    def __init__(self):
        logger.info("hello mom...")
        super(ManholeFactory, self).__init__()
#        self.clients = {}
        
#    def buildProtocol(self, addr):
#        proto = telnet.ShellFactory(buildProtocol(addr))
#        self.client = proto
#        return proto
            
class Shell(telnet.Shell):
    def welcomeMessage(self):
        return """
  
        ************************
        * Yombo Manhole Module *
        ************************
  
"""
